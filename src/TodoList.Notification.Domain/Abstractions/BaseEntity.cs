﻿namespace TodoList.Notification.Domain.Abstractions
{
    /// <summary>
    /// Базовый класс сущности
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Время удаления сущности (UTC)
        /// </summary>
        public DateTime? Deleted { get; private set; }

        /// <summary>
        /// Время создания сущности (UTC)
        /// </summary>
        public DateTime Created { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseEntity"/> class
        /// </summary>
        protected BaseEntity()
            : base()
        {
            Deleted = null;
            Created = DateTime.UtcNow;
        }

        /// <summary>
        /// Удаление сущности
        /// </summary>
        /// <param name="deleted">Время удаления сущности (UTC)</param>
        public virtual void SetDeleted(DateTime? deleted)
        {
            if (Deleted == deleted)
            {
                return;
            }

            Deleted = deleted;
        }
    }
}
