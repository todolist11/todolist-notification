﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Notification.Infrastructure.Exceptions
{
    /// <summary>
    /// Исключение уровня инфраструктуры (NotFound)
    /// </summary>
    public class NotFoundInfrastructureException : InfrastrcutureException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundInfrastructureException"/> class
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        public NotFoundInfrastructureException(int id)
            : base($"Не удалось найти сущность с Id {id}")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundInfrastructureException"/> class.
        /// </summary>
        public NotFoundInfrastructureException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundInfrastructureException"/> class.
        /// </summary>
        /// <param name="message">Сообщение</param>
        public NotFoundInfrastructureException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundInfrastructureException"/> class.
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="innerException">Исключение</param>
        public NotFoundInfrastructureException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
