﻿using Microsoft.EntityFrameworkCore;
using TodoList.Notification.Infrastructure;

namespace TodoList.Notification.Api.Extensions
{
    internal static class MigratesExtensions
    {
        public static async Task AutoMigrate(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var dataContext = scope.ServiceProvider.GetRequiredService<NotificationsContext>();
                await dataContext.Database.MigrateAsync();
            }
        }
    }
}
